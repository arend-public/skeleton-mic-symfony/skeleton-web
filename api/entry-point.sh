#!/bin/sh

## shellcheck disable=SC2006
#IP=`ip addr | grep -E 'eth0.*state UP' -A2 | tail -n 1 | awk '{print $2}' | cut -f1 -d '/'`
#CHECK_ROUTE="/new-api/"
#NAME="new-api"
#PORT=80
#
#read -r -d '' MSG << EOM
#{
#  "Name": "$NAME",
#  "address": "$IP",
#  "port": $PORT,
#  "Check": {
#     "http": "http://$IP:$PORT$CHECK_ROUTE",
#     "interval": "5s"
#  }
#}
#EOM
#
#curl -v -XPUT -d "$MSG" http://docker_consul_1:8500/v1/agent/service/register

php-fpm -D -R
nginx