import home from "../views/home.vue";
export default [
  {
    path: '/',
    name: 'Home',
    slug: 'home',
    component: home,
    meta: {
      showInHeader: true,
    },
  },
];
